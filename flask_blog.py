from flask import Flask, render_template, url_for, flash, redirect
from forms import RegistrationForm, LoginForm

app = Flask(__name__)

app.config['SECRET_KEY'] = '562b2e711d9b60ef73bc7c842278e0aa'
posts = [
    {
        'author': 'Gowtham Rangan',
        'title': 'Blog Post 1',
        'content': 'First post content',
        'date_posted': 'Jul 20, 2018'
    },
    {
        'author': 'Gayathri Mohan',
        'title': 'Blog Post 2',
        'content': 'Second post content',
        'date_posted': 'Jul 21, 2018'
    }
]


@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', posts=posts)


@app.route("/about")
def about():
    return render_template('about.html', title='About')


@app.route("/login")
def login():
    form = LoginForm()
    return render_template('login.html', title='Login', form = form)

@app.route("/registration", methods=['GET','POST'])
def registration():
    form = RegistrationForm()
    if form.validate_on_submit():
        flash(f'Account created for the user {form.username.data}', 'success')
        return redirect(url_for('home'))
    return render_template('registration.html', title='Registration', form = form)

if __name__ == '__main__':
    app.run(debug=True)
